#ifndef CENTER_HPP
#define CENTER_HPP

#include "Geometry.hpp"

#include <vector>
#include <cassert>
#include <iterator>
#include <type_traits>
#include <limits>
#include <algorithm>
#include <utility>

// private api - necessary since I'm using template functions
namespace center_impl
{
/*
gets enscribed circle from 3 points

The math comes from solving the intersection between the
perpendicular bisectors of p1->p2 and p2->p3
Adapted from the formula's here http://www.ambrsoft.com/TrigoCalc/Circle3D.htm
*/
	inline circle enscribedCircle(const vec2& p1, const vec2& p2, const vec2& p3)
	{
		float a = p1.x * (p2.y - p3.y) - p1.y * (p2.x - p3.x) + p2.x * p3.y - p3.x * p2.y;

		float b = (p1.x * p1.x + p1.y * p1.y) * (p3.y - p2.y)
			+ (p2.x * p2.x + p2.y * p2.y) * (p1.y - p3.y)
			+ (p3.x * p3.x + p3.y * p3.y) * (p2.y - p1.y);

		float c = (p1.x * p1.x + p1.y * p1.y) * (p2.x - p3.x)
			+ (p2.x * p2.x + p2.y * p2.y) * (p3.x - p1.x)
			+ (p3.x * p3.x + p3.y * p3.y) * (p1.x - p2.x);

		vec2 center{-b / (2 * a), -c / (2 * a)};
		return circle{center, (p1 - center).length()};
	}

	/*
	gets the minimum circle around 1, 2, or 3 points

	The 3 scenarios are all trivial, with the 3 pointer defering to the above function
	*/
	inline circle minimumCircle3(const std::vector<vec2>& boundary)
	{
		if (boundary.size() == 0)
			return circle{}; // empty circle about origin
		else if (boundary.size() == 1)
			return circle{boundary[0]}; // circle of zero radius about point
		else if (boundary.size() == 2)
		{
			vec2 center = (boundary[0] + boundary[1]) / 2.f;	// average position
			float radius = (boundary[1] - boundary[0]).length() / 2.f;	// half the distance
			return circle{center, radius};
		}
		else
		{
			assert(boundary.size() == 3);	// unexpected function misuse

			// see if we can get away with 2 points
			circle test = minimumCircle3({boundary[1], boundary[2]});
			if (test.contains(boundary[0]))
				return test;
			test = minimumCircle3({boundary[0], boundary[2]});
			if (test.contains(boundary[1]))
				return test;
			test = minimumCircle3({boundary[0], boundary[1]});
			if (test.contains(boundary[2]))
				return test;
			return enscribedCircle(boundary[0], boundary[1], boundary[2]);
		}
	}

	/*
	Welzl's algorithm for the smallest circle about a set of points
	with "boundary" being the known boundary points

	This recursive algorithm solves for a subset, and asks if the
	first point is inside the circle.
	If it is, then return the subset's solution
	If it's not, do the subset again, but adding that point to the boundary knowns

	Once we have 3 points in a boundary, or run out of points
	we can defer to a trivial solution with the above function
	*/
	template<typename Iter>
	circle welzl(Iter begin, Iter end, std::vector<vec2> boundary)
	{
		if (begin == end || boundary.size() == 3)
			return minimumCircle3(boundary);
		circle test = welzl(std::next(begin), end, boundary);
		if (test.contains(*begin))
			return test;
		boundary.push_back(*begin);
		return welzl(std::next(begin), end, boundary);
	}
}

/*
Finds the smallest circle about an arbitrary number of points

typename Iter - an iterator type of value_type (const) vec2

begin - point sequence begin iterator
end - point sequence end iterator

return - the smallest circle
*/
template<typename Iter>
circle smallestCircle(Iter begin, Iter end)
{
	using value_type = std::remove_cv_t<typename std::iterator_traits<Iter>::value_type>;	// const allowed
	static_assert(std::is_same_v<value_type, vec2>, "smallestCircle requires iterator to vec2");
	
	return center_impl::welzl(begin, end, {});
}

/*
In-place centers a sequence of models on a plate

typename Iter - an iterator type of value_type (non-const) pair<const Model*, aabb>

begin - the model sequence begin iterator
end - the model sequence end iterator

return - the smallest circle around the models
*/
template<typename Iter>
circle centerModels(Iter begin, Iter end, aabb plate)
{
	using value_type = typename std::iterator_traits<Iter>::value_type;
	static_assert(std::is_same_v<value_type, std::pair<const Model*, aabb>>, "centerModels requires iterator to pair<Model*, aabb>");
	float inf = std::numeric_limits<float>::infinity();
	float minX = inf;
	float minY = inf;
	float maxX = -inf;
	float maxY = -inf;
	std::vector<vec2> points;

	for (auto iter = begin; iter != end; ++iter)
	{
		// find the absolute minimum/maximum coordinates
		auto itemMin = iter->second.center - iter->second.radii;
		auto itemMax = iter->second.center + iter->second.radii;
		minX = std::min(minX, itemMin.x);
		minY = std::min(minY, itemMin.y);
		maxX = std::max(maxX, itemMax.x);
		maxY = std::max(maxY, itemMax.y);

		// generate a list of points so we can encircle->center them
		points.push_back({itemMin.x, itemMin.y});
		points.push_back({itemMin.x, itemMax.y});
		points.push_back({itemMax.x, itemMin.y});
		points.push_back({itemMax.x, itemMax.y});
	}
	vec2 minOffset{-plate.radii.x - minX, -plate.radii.y - minY};
	vec2 maxOffset{plate.radii.x - maxX, plate.radii.x - maxY};

	// the smallest circle is what we aim to center
	circle bounds = smallestCircle(points.begin(), points.end());

	// but we need to clamp it in case centering moves models off the plate
	vec2 offset = -bounds.center;
	offset.x = std::clamp(offset.x, minOffset.x, maxOffset.x);
	offset.y = std::clamp(offset.y, minOffset.y, maxOffset.y);
	offset += plate.center;	// offset gets us to origin, so add the plate center to that
	bounds.center += offset;	// recenter the returned circle as well

	// now go through and adjust every position
	for (auto iter = begin; iter != end; ++iter)
		iter->second.center += offset;

	return bounds;
}

#endif
