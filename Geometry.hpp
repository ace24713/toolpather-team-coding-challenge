#ifndef GEOMETRY_HPP
#define GEOMETRY_HPP

#include <cmath>


/*
vec2 - simple 2d position structure

I'd normally use some external math library for a class like this,
but it's easy enough to write my own for the purposes of this assignment

Includes some useful operators and methods
*/
struct vec2
{
	float x = 0.f;
	float y = 0.f;

	float lengthSquared() const
	{
		return x * x + y * y;
	}

	float length() const
	{
		return std::sqrt(lengthSquared());
	}

	// addition assignment
	vec2& operator+=(const vec2& rhs)
	{
		x += rhs.x;
		y += rhs.y;
		return *this;
	}

	// subtraction assignment
	vec2& operator-=(const vec2& rhs)
	{
		x -= rhs.x;
		y -= rhs.y;
		return *this;
	}

	// multiply assignment
	vec2& operator*=(float rhs)
	{
		x *= rhs;
		y *= rhs;
		return *this;
	}

	// divide assignment
	vec2& operator/=(float rhs)
	{
		x /= rhs;
		y /= rhs;
		return *this;
	}

	// unary negate
	friend vec2 operator-(const vec2& rhs) { return vec2{-rhs.x, -rhs.y}; }

	// binary addition
	friend vec2 operator+(vec2 lhs, const vec2& rhs) { return lhs += rhs; }

	// binary subtraction
	friend vec2 operator-(vec2 lhs, const vec2& rhs) { return lhs -= rhs; }

	// binary multiply
	friend vec2 operator*(vec2 lhs, float rhs) { return lhs *= rhs; }
	friend vec2 operator*(float lhs, vec2 rhs) { return rhs *= lhs; }

	// binary divide
	friend vec2 operator/(vec2 lhs, float rhs) { return lhs /= rhs; }
	friend vec2 operator/(float lhs, vec2 rhs) { return rhs /= lhs; }

	// equivalence
	friend bool operator==(const vec2& lhs, const vec2& rhs) { return lhs.x == rhs.x && lhs.y == rhs.y; }
	friend bool operator!=(const vec2& lhs, const vec2& rhs) { return !(lhs == rhs); }
};

/*
Model data type

only has a size and id
but presumably there would be other features here in a larger codebase

unlike the other classes, size here is not radial size, but width x height
because these are generally presented to the api user, and it's more intuitive
*/
struct Model
{
	std::size_t id;
	vec2 size;
};

/*
aabb - axis aligned bounding box

Uses center - radii format because it's easy to do math with that
*/
struct aabb
{
	vec2 center = vec2{};
	vec2 radii = vec2{};	// technically radii are for circles, but it's an apt name

	// test if an aabb contains a point (boundary inclusive)
	bool contains(const vec2& p) const
	{
		vec2 rel = (p - center);
		return std::abs(rel.x) <= radii.x && std::abs(rel.y) <= radii.y;
	}

	// test if an aabb contains another aabb (boundary inclusive)
	bool contains(const aabb& b) const
	{
		return contains(b.center + b.radii) && contains(b.center - b.radii);
	}

	// test if an aabb intsersects another aabb (boundary exclusive)
	// only intersect if my lower left is below and left of their upper right and vice-versa
	bool intersects(const aabb& b) const
	{
		vec2 mina = center - radii;
		vec2 maxa = center + radii;
		vec2 minb = b.center - b.radii;
		vec2 maxb = b.center + b.radii;
		return mina.x < maxb.x && mina.y < maxb.y && minb.x < maxa.x && minb.y < maxa.y;
	}
};

/*
circle - represents a 2d circle

Also uses center - radius format
*/
struct circle
{
	vec2 center = vec2{};
	float radius = 0.f;

	// test if circle contains point (boundary inclusive)
	bool contains(const vec2& p) const
	{
		return (p - center).lengthSquared() <= radius * radius;
	}
};

#endif
