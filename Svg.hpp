#ifndef SVG_HPP
#define SVG_HPP

#include "Geometry.hpp"

#include <fstream>
#include <string>

/*
SvgFile class provides easy generation of svg file for viewing geometry

I used this mostly for my own debugging

can draw circles and rectangles (aabbs)
*/
class SvgFile
{
private:
	// tag types. I need unique types for stream modifiers later
	struct FillColor { std::string color; };
	struct StrokeColor { std::string color; };
public:

	/*
	open an svg file

	filename - the filename.svg
	viewbox - defines the "camera" for svg viewers. Should be centered, and extend past the edge of the image a bit
	*/
	SvgFile(std::string filename, aabb viewbox) : 
		_file{std::move(filename)}
	{
		if (_file)
		{
			vec2 minCorner = viewbox.center - viewbox.radii;
			vec2 size = 2.f * viewbox.radii;

			_file << R"(<?xml version="1.0" encoding="UTF-8"?>)" << '\n';
			_file << R"(<svg xmlns="http://www.w3.org/2000/svg" )";
			_file << R"(width=")" << size.x << R"(" height=")" << size.y << R"(" )";
			_file << R"(viewBox=")" << minCorner.x << ' ' << minCorner.y << ' ' << size.x << ' ' << size.y << R"(" >)" << '\n';
		}
	}

	/*
	manually close the svg file
	*/
	void close()
	{
		if (_file)
		{
			_file << R"(</svg>)" << '\n';
			_file.close();
		}
	}

	~SvgFile() { close(); }

	// can't copy ofstream
	SvgFile& operator=(const SvgFile&) = delete;
	SvgFile(const SvgFile&) = delete;

	// default move operation works fine for us, despite inclusion of a destructor
	SvgFile& operator=(SvgFile&&) = default;
	SvgFile(SvgFile&&) = default;

	/*
	stream modifiers. Called like:
		file << setFill("red") << ...
	to set the fill. Similar to how stream modifiers work with cout, i.e. std::setw
	*/
	friend FillColor setFill(std::string color);
	friend StrokeColor setStroke(std::string color);
	friend FillColor clearFill();
	friend StrokeColor clearStroke();
	friend SvgFile& operator<<(SvgFile& out, FillColor color)
	{
		out._currentFill = std::move(color.color);
		return out;
	}
	friend SvgFile& operator<<(SvgFile& out, StrokeColor color)
	{
		out._currentStroke = std::move(color.color);
		return out;
	}

	// draw a circle with the current fill and stroke settings
	friend SvgFile& operator<<(SvgFile& out, const circle& circle)
	{
		out._file << R"(<circle )";
		if (!out._currentFill.empty())
			out._file << R"(fill=")" << out._currentFill << R"(" )";
		if (!out._currentStroke.empty())
			out._file << R"(stroke=")" << out._currentStroke << R"(" )";
		out._file << R"(r=")" << circle.radius << R"(" )";
		out._file << R"(cx=")" << circle.center.x << R"(" cy=")" << circle.center.y << R"(" />)" << '\n';
		return out;
	}

	// draw a rectangle with the current fill and stroke settings
	friend SvgFile& operator<<(SvgFile& out, const aabb& rect)
	{
		vec2 minCorner = rect.center - rect.radii;
		vec2 size = 2.f * rect.radii;
		out._file << R"(<rect )";
		if (out._currentFill.empty())
			out._file << R"(fill-opacity="0.0" )";
		else
			out._file << R"(fill=")" << out._currentFill << R"(" )";
		if (!out._currentStroke.empty())
			out._file << R"(stroke=")" << out._currentStroke << R"(" )";
		out._file << R"(width=")" << size.x << R"(" height=")" << size.y << R"(" )";
		out._file << R"(x=")" << minCorner.x << R"(" y=")" << minCorner.y << R"(" />)" << '\n';
		return out;
	}

private:
	std::string _currentFill;
	std::string _currentStroke;

	std::ofstream _file;
};


// stream modifiers implemented like simple constructors
inline SvgFile::FillColor setFill(std::string color) { return SvgFile::FillColor{std::move(color)}; }
inline SvgFile::StrokeColor setStroke(std::string color) { return SvgFile::StrokeColor{std::move(color)}; }
inline SvgFile::FillColor clearFill() { return SvgFile::FillColor{}; }
inline SvgFile::StrokeColor clearStroke() { return SvgFile::StrokeColor{}; }

#endif
