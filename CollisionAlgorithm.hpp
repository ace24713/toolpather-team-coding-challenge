#ifndef COLLISION_ALGORITHM_HPP
#define COLLISION_ALGORITHM_HPP

#include "Geometry.hpp"
#include "Center.hpp"


// private api - necessary since I'm using template functions
namespace collision_impl
{
	/*
	get the first colliding model

	pretty straightforward, just iterate and return the first one, or end
	*/
	template<typename Iter>
	Iter getCollider(const aabb& item, Iter begin, Iter end)
	{
		for (; begin != end; ++begin)
			if (begin->second.intersects(item))
				return begin;
		return end;
	}

	/*
	place an item on the plate

	the bulk of this algorithm relies on this recursive function
	basically it finds a collider, and slides it until it doesn't collide anymore, then repeat
	we only slide in outward directions

	bestPlace(in/out) - the current best position. We can stop if we know we'll exceed this distance
	maxPlace - essentially the radial plate dimensions, we can also stop if we exceed this
	item - what we're placing
	begin - the start of the sequence of already placed items
	end - the end of the sequence of already placed items
	epsilon - fudge factor to prevent floating point overlap after collision
	*/
	template<typename Iter>
	void placeItem(vec2& bestPlace, const vec2& maxPlace, const aabb& item, Iter begin, Iter end, float epsilon)
	{
		// verify we're not already too far out
		if (item.center.lengthSquared() >= bestPlace.lengthSquared()
			|| std::abs(item.center.x) + item.radii.x > maxPlace.x
			|| std::abs(item.center.y) + item.radii.y > maxPlace.y)
			return;
		auto collider = getCollider(item, begin, end);
		if (collider == end)
			bestPlace = item.center;	// we're not colliding with anything, so here is good
		if (item.center.x >= 0.f)	// slide to the right
		{
			aabb test = item;
			test.center.x = collider->second.center.x;
			test.center.x += item.radii.x + collider->second.radii.x + epsilon;
			placeItem(bestPlace, maxPlace, test, begin, end, epsilon);
		}
		if (item.center.y >= 0.f)	// slide up
		{
			aabb test = item;
			test.center.y = collider->second.center.y;
			test.center.y += item.radii.y + collider->second.radii.y + epsilon;
			placeItem(bestPlace, maxPlace, test, begin, end, epsilon);
		}
		if (item.center.x <= 0.f)	// sliiiiiide to the left
		{
			aabb test = item;
			test.center.x = collider->second.center.x;
			test.center.x -= item.radii.x + collider->second.radii.x + epsilon;
			placeItem(bestPlace, maxPlace, test, begin, end, epsilon);
		}
		if (item.center.y <= 0.f)	// slide down
		{
			aabb test = item;
			test.center.y = collider->second.center.y;
			test.center.y -= item.radii.y + collider->second.radii.y + epsilon;
			placeItem(bestPlace, maxPlace, test, begin, end, epsilon);
		}
	}
}

/*
Collision algrithm was something I dreamed up to get tighter packing on low number of models
It's efficiency is pretty poor, but it'll be way more tightly packed
It takes blocks one at a time and tries to place them as close to the center as possible.
It does so by trying a position, and detecting collisions
from a collision, we generate more positions to try that aren't colliding with that object
and it'll take the closest-to-the-center legal placement it can get

since I take models one-by-one it inherently won't get the perfect placement,
to mitigate this, I have a misalign option that shifts the position of the first piece.
This will sometimes get a better configuration...
It will *always* get a better configuration for exactly 3 models though.
And since 3 models is a likely use-case I figured I'd add the option.
For low number of models, we can try both...
for high number of models misalignment doesn't make a big difference


typename Iter - an iterator type of value_type (non-const) pair<const Model*, aabb>

begin - the model sequence begin iterator
end - the model sequence end iterator
plate - the plate the models must fit on
misalign - intentionally misplace the first model to get a different result (see above)
epsilon - models are seperated by this amount to prevent re-collision

return - the smallest circle around the models
throws - length_error if failed to arrange models
*/
template<typename Iter>
auto collisionAlgorithm(Iter begin, Iter end, aabb plate, bool misalign = false, float epsilon = 0.00001f)
{
	using value_type = typename std::iterator_traits<Iter>::value_type;
	static_assert(std::is_same_v<value_type, std::pair<const Model*, aabb>>, "collisionAlgorithm requires iterator to pair<Model*, aabb>");

	// center all models
	for (auto iter = begin; iter != end; ++iter)
		iter->second.center = {};

	// sort by area - largest first
	std::sort(begin, end, [](const value_type& lhs, const value_type& rhs)
		{
			return std::max(lhs.second.radii.x, lhs.second.radii.y) > std::max(rhs.second.radii.x, rhs.second.radii.y);
			//return lhs.second.radii.x*lhs.second.radii.y > rhs.second.radii.x*rhs.second.radii.y;
		});

	if (misalign)
	{
		std::swap(*begin, *std::next(begin));	// we want the biggest piece closest to the center, so take the 2nd biggest instead
		begin->second.center += begin->second.radii;	// place with the corner being in the center
	}

	for (auto iter = begin; iter != end; ++iter)
	{
		vec2 infinity = {std::numeric_limits<float>::infinity(), std::numeric_limits<float>::infinity()};

		// I have two possible orientations to try, so we do this twice
		vec2 bestPlaceA = infinity;
		collision_impl::placeItem(bestPlaceA, plate.radii, iter->second, begin, iter, epsilon);

		std::swap(iter->second.radii.x, iter->second.radii.y);	// rotate

		vec2 bestPlaceB = bestPlaceA;
		collision_impl::placeItem(bestPlaceB, plate.radii, iter->second, begin, iter, epsilon);

		if (bestPlaceB == infinity)
			throw std::length_error{"can't fit objects on plate"};
		if (bestPlaceB == bestPlaceA)
			std::swap(iter->second.radii.x, iter->second.radii.y);	// rotate back

		iter->second.center = bestPlaceB;	// set position
	}
	return centerModels(begin, end, plate);
}

#endif
