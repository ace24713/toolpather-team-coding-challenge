#include "Geometry.hpp"
#include "Svg.hpp"
#include "Center.hpp"
#include "ShelfAlgorithm.hpp"
#include "CollisionAlgorithm.hpp"

#include <cctype>
#include <iostream>
#include <random>

/*
layout the models as close to the origin as possible using the best of 3 different algorithms
Info on the respective approaches can be found in their header files

typename Iter - an iterator type of value_type (const) Model

begin - the begin iterator of the sequence of models
end - the end iterator of the sequence of models
plateDimensions - the length x width of the base plate to arrange the models on

return - vector pairing each model* to the aabb position it should occupy.
			it will be in arbitrary order
			the orientation (portrait/landscape) of the output aabb will not necessarily match the model
*/
template<typename Iter>
std::vector<std::pair<const Model*, aabb>> layoutModels(Iter begin, Iter end, vec2 plateDimensions)
{
	using iter_value_type = std::remove_cv_t<typename std::iterator_traits<Iter>::value_type>;	// const allowed
	static_assert(std::is_same_v<iter_value_type, Model>, "layoutModels requires iterator to Model");

	// convert to output form-factor for our algorithms
	std::vector<std::pair<const Model*, aabb>> input;
	for (; begin != end; ++begin)
		input.push_back(std::make_pair(&*begin, aabb{{}, begin->size / 2.f}));	// aabb radii is half model dimensions

	aabb plate{{}, plateDimensions / 2.f};	// convert plate dimensions to radii

	std::vector<std::pair<const Model*, aabb>> bestOutput;
	circle bestCircle{{}, std::numeric_limits<float>::infinity()};

	// shelf algorithm. Always fast, usually not the best solution
	try
	{
		auto shelfOutput = input;	// copy vector, because we'll modify it
		auto shelfCircle = shelfAlgorithm(shelfOutput.begin(), shelfOutput.end(), plate);
		bestOutput = std::move(shelfOutput);
		bestCircle = shelfCircle;
	}
	catch (std::exception& err)
	{
		std::cerr << "shelf algorithm failed with: " << err.what() << std::endl;
	}

	// collision algorithm. Starts to get slow around ~80 models in my testing
	try
	{
		if (input.size() <= 80)
		{
			auto collisionOutput = input;
			auto collisionCircle = collisionAlgorithm(collisionOutput.begin(), collisionOutput.end(), plate);
			if (collisionCircle.radius < bestCircle.radius)
			{
				bestOutput = std::move(collisionOutput);
				bestCircle = collisionCircle;
			}
		}
	}
	catch (std::exception& err)
	{
		std::cerr << "collision algorithm failed with: " << err.what() << std::endl;
	}

	// misaligned collision algorithm. Worth trying for small numbers of models.
	// solution will tend to approach the regular collision algorithm with enough models.
	try
	{
		if (input.size() <= 50)
		{
			auto misalignedOutput = input;
			auto misalignedCircle = collisionAlgorithm(misalignedOutput.begin(), misalignedOutput.end(), plate, true);
			if (misalignedCircle.radius < bestCircle.radius)
			{
				bestOutput = std::move(misalignedOutput);
				bestCircle = misalignedCircle;
			}
		}
	}
	catch (std::exception& err)
	{
		std::cerr << "misaligned collision algorithm failed with: " << err.what() << std::endl;
	}

	return bestOutput;
}

/*
generate random models using uniform distribution

typename Generator - random generation engine type

minSize - the minimum size of a model
maxSize - the maximum size of a model
maxCount - the maximum number of models to generate (will be less if they won't fit)
safeDimensions - the max amount of space we should take up on the build plate
					the higher this number, the more likely the algorithms will fail to place models
gen - random number generator

return - vector of generated models, id increments from 0
*/
template<typename Generator>
std::vector<Model> randomModels(vec2 minSize, vec2 maxSize, std::size_t maxCount, vec2 safeDimensions, Generator& gen)
{
	float areaRemaining = safeDimensions.x * safeDimensions.y;

	std::uniform_real_distribution<float> getx{minSize.x, maxSize.x};
	std::uniform_real_distribution<float> gety{minSize.y, maxSize.y};
	std::vector<Model> output;
	for (std::size_t i = 0; i < maxCount; ++i)
	{
		vec2 size{std::abs(getx(gen)), std::abs(gety(gen))};
		areaRemaining -= size.x * size.y;
		if (areaRemaining < 0.f)
			break;
		output.push_back(Model{i, size});
	}
	return output;
}


/*
main function

deals with all the user io
includes generation of svg file output for viewing/debugging pleasure

return - 0 on success, 1 on failure to place models
*/
int main()
{
	vec2 plateDimensions{};
	std::default_random_engine gen;
	std::vector<Model> models;

	std::cout << R"(enter build plate dimensions "width height")" << std::endl;
	std::cin >> plateDimensions.x >> plateDimensions.y;

	while (true)
	{
		std::string method;
		std::cout << R"(enter model generation mode "manual" "random" or "seeded")" << std::endl;
		std::cin >> method;
		std::transform(method.begin(), method.end(), method.begin(), std::tolower);
		if (method == "manual")
		{
			std::size_t count = 0;
			std::cout << "enter count" << std::endl;
			std::cin >> count;
			for (std::size_t i = 0; i < count; ++i)
			{
				Model m{i, vec2{}};
				std::cout << "enter model " << i << R"( info "width height")" << std::endl;
				std::cin >> m.size.x >> m.size.y;
				models.push_back(std::move(m));
			}
			break;
		}
		else if (method == "random")
		{
			auto seed = std::random_device{}();
			std::cout << "seeding random generator with " << seed << std::endl;
			gen = std::default_random_engine{seed};
			break;
		}
		else if (method == "seeded")
		{
			unsigned int seed = 0;
			std::cout << "enter seed as an unsigned int" << std::endl;
			std::cin >> seed;
			std::cout << "seeding random generator with " << seed << std::endl;
			gen = std::default_random_engine{seed};
			break;
		}
		else
			std::cout << "invalid model generation mode" << std::endl;
	}

	if (models.empty())
	{
		std::size_t maxCount = 0;
		float proportion = 0.f;
		std::cout << "enter maximum model count" << std::endl;
		std::cin >> maxCount;
		std::cout << R"(enter maximum proportion of used space "0.0" to "1.0")" << std::endl;
		std::cin >> proportion;
		proportion = std::clamp(proportion, 0.f, 1.f);
		
		// divide up the space, and approximate deviations of 50%... should be okay for testing
		vec2 safeSpace = plateDimensions * proportion;
		float approximateEdge = std::sqrt((safeSpace.x*safeSpace.y) / static_cast<float>(maxCount));
		float deviation = approximateEdge / 2.f;
		float min = approximateEdge - deviation;
		float max = approximateEdge + deviation;
		models = randomModels({min, min}, {max, max}, maxCount, safeSpace, gen);
		std::cout << "generated " << models.size() << " models" << std::endl;
	}

	auto result = layoutModels(models.cbegin(), models.cend(), plateDimensions);
	
	if (result.empty())
	{
		std::cout << "failed to fit models on plate" << std::endl;
		return 1;
	}

	std::cout << "completed laying out models with:" << std::endl;
	for (const auto& item : result)
	{
		vec2 minCorner = item.second.center - item.second.radii;
		vec2 maxCorner = item.second.center + item.second.radii;
		std::cout << "model " << item.first->id << " at rectangle"
			<< " left: " << minCorner.x << " bottom: " << minCorner.y
			<< " right: " << maxCorner.x << " top: " << maxCorner.y << std::endl;
	}

	std::cout << std::endl << "generating output.svg" << std::endl;

	SvgFile display{"output.svg", aabb{{}, plateDimensions}}; // view zoomed out to twice the plate radii
	display << setFill("green") << setStroke("black");
	display << aabb{{}, plateDimensions / 2.f};
	display << setFill("white") << setStroke("black");
	for (const auto& item : result)
		display << item.second;

	std::cout << "done!" << std::endl;
	return 0;
}
