#ifndef SHELF_ALGORITHM_HPP
#define SHELF_ALGORITHM_HPP

#include "Geometry.hpp"
#include "Center.hpp"

#include <limits>
#include <utility>
#include <vector>
#include <algorithm>
#include <stdexcept>

// private api - necessary since I'm using template functions
namespace shelf_impl
{
	/*
	shelf is a recursive class containing aabb's
	imagine a bookshelf, but whenever you add a book, you add a shelf directly above it

	The shelf only represents positive space, so to make up for it it also knows it's quadrant
	then, when inserting aabb's they are positioned using real coordinates
	instead of the relative coordinates I use to make the math more intuitive
	*/
	class shelf
	{
	public:
		/*
		a shelf is constructed from empty space, and a quadrant

		space - where the bookshelf is in absolute value space
		invertX - is the shelf in a negative x-value quadrant
		invertY - is the shelf in a negative y-value quadrant
		*/
		shelf(aabb space, bool invertX, bool invertY) :
			_space{space}, _invertX{invertX}, _invertY{invertY}
		{}

		/*
		query where to place an aabb into this shelf, or subshelves

		box - the aabb to place on the shelf

		returns - the weight (i.e. distance^2 from center) of adding the book | and the subshelf pointer to add to
		*/
		std::pair<float, shelf*> testInsert(const aabb& box)
		{
			std::pair<float, shelf*> best;
			if (box.radii.x <= _space.radii.x && box.radii.y <= _space.radii.y) // if it fits here
			{
				float weight = (_space.center - _space.radii + box.radii * 2.f).lengthSquared();
				best = std::make_pair(weight, this);
			}
			else
				best = std::make_pair(std::numeric_limits<float>::infinity(), nullptr);	// failure is null

			// recurse on subshelves
			for (auto& subshelf : _items)
			{
				auto test = subshelf.second.testInsert(box);
				if (test.first < best.first)
					best = test;
			}
			return best;
		}

		/*
		insert an aabb into this shelf (not subshelf), and reposition it as such
		since it forces the aabb in, do a testInsert first to see if and where to fit it

		box - what to place on the shelf
		*/
		void directInsert(aabb& box)
		{
			// box will be moved into position
			box.center = _space.center - _space.radii + box.radii;

			aabb subshelfSpace = _space;
			subshelfSpace.center.y += box.radii.y;
			subshelfSpace.radii.y -= box.radii.y;
			_space.center.x += box.radii.x;
			_space.radii.x -= box.radii.x;
			// adjust remaining space now with shelf on top
			_space.radii.y = box.radii.y;
			_space.center.y = box.center.y;
			if (_invertX)
				box.center.x *= -1.f;
			if (_invertY)
				box.center.y *= -1.f;
			_items.emplace_back(&box, shelf{subshelfSpace, _invertX, _invertY});
		}

	private:
		aabb _space;
		bool _invertX;
		bool _invertY;
		std::vector<std::pair<const aabb*, shelf>> _items;
	};
}

/*
Shelf Algorithm arrange models on a plate

this is a variation of the recursive shelf 2d bin packing algorithm
It arranges models on shelves (strict horizontal lines) and in order of decreasing height
The "variation" is that it divides into 4 quadrant shelves,
and decides where to put it based on how close it can place to the center

It's better designed for speed than best-fit packing
but it was the first solution I came up with, so I left it in as extra credit

typename Iter - an iterator type of value_type (non-const) pair<const Model*, aabb>

begin - the model sequence begin iterator
end - the model sequence end iterator
plate - the plate the models must fit on

return - the smallest circle around the models
throws - length_error if failed to arrange models
*/
template<typename Iter>
auto shelfAlgorithm(Iter begin, Iter end, aabb plate)
{
	using value_type = typename std::iterator_traits<Iter>::value_type;
	static_assert(std::is_same_v<value_type, std::pair<const Model*, aabb>>, "shelfAlgorithm requires iterator to pair<Model*, aabb>");

	// orient all models in portrait and center them
	for (auto iter = begin; iter != end; ++iter)
	{
		iter->second.center = {};
		if (iter->second.radii.x < iter->second.radii.y)
			std::swap(iter->second.radii.x, iter->second.radii.y);
	}
	
	// sort by decreasing height
	std::sort(begin, end, [](value_type& lhs, value_type& rhs)
		{
			if (lhs.second.radii.y == rhs.second.radii.y)
				return lhs.second.radii.x > rhs.second.radii.x;
			return lhs.second.radii.y > rhs.second.radii.y;
		});

	vec2 shelfRadii = plate.radii / 2.f;
	aabb shelfRegion{ shelfRadii, shelfRadii };

	// 4 primary shelves, one for each quadrant about the origin
	shelf_impl::shelf shelfI{shelfRegion, false, false};
	shelf_impl::shelf shelfII{shelfRegion, true, false};
	shelf_impl::shelf shelfIII{shelfRegion, true, true};
	shelf_impl::shelf shelfIV{shelfRegion, false, true};

	for (auto iter = begin; iter != end; ++iter)
	{
		// try it in all quadrants and pick the best result
		auto best = shelfI.testInsert(iter->second);
		auto test = shelfII.testInsert(iter->second);
		if (test.first < best.first)
			best = test;
		test = shelfIII.testInsert(iter->second);
		if (test.first < best.first)
			best = test;
		test = shelfIV.testInsert(iter->second);
		if (test.first < best.first)
			best = test;

		if (!best.second)	// if we got null, it doesn't fit anywhere
			throw std::length_error{"can't fit objects on plate"};

		// actually place it
		best.second->directInsert(iter->second);
	}

	// finally, center the models about the plate, and return the circle for comparison
	return centerModels(begin, end, plate);
}

#endif
