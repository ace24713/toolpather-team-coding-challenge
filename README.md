# Toolpather Team Coding Challenge

My solution to an auto-layout algorithm coding challenge

## Original Background

Let’s assume a 3D printer creates objects by depositing plastic on a surface along the z-axis.
Starting from the bottom and working upwards in z-axis, more plastic is added to the model
through an extruder which creates layer on top of layer until the object is fully-formed.
Before models can be printed, they must be arranged on a plane that represents the build plate
upon which the printer will create the models. We know from print quality testing and the
mechanics of our printers that models closer to the center of the build plate are more likely to
have better quality than models printed near the edges of the build plate.
As users load each model file into our desktop application, we should arrange models around the
enter of our buildplate in the most optimized way possible.

## Original Challenge

Create a C++ algorithm that will take a collection of models and arrange them optimally on a
build plate.
The center of the build plate should be the origin (0, 0). The algorithm should be able to take on
any build plate dimension and plate models within the bounds.
Each model can be represented by an axis-aligned bounding box taking into account length and
width only, and excluding height. How a model placement on the buildplate is stored is up to
you.
The function containing the algorithm should be templated to take any container model, for
example: std::deque<Model> or std::vector<Model>.
You are free to use msvc, gcc, or clang to compile your code. Please include instructions for
compilation and execution in your solution.

## Project Notes

My interpretation of this challenge was to create an algorithm that places each model as close to the center of a build plate as possible.
The most optimal solution would be a very tightly packed circle.
This is effectively a variation of the 2d bin packing problem, except more difficult because:
*  I'm finding the minimum circular layout rather than being given a fixed "bin" of space
*  Models use floating point offsets, so it's completely unreasonable to enumerate all configurations

I initially tried to find the most optimal solution, but that turned out to be impractical.
So I instead found 2.5 good solutions, and try them all.
These algorithms place items one at a time, attempting to place the next item optimally, so they can't find the perfect solution.
The perfect solution would require knowing all information at once.

## Compilation Instructions

I compiled this code using the latest visual studio 2019 preview, but it *should* work on any c++17 conforming compiler.

From a [visual studio developer command prompt](https://docs.microsoft.com/en-us/dotnet/framework/tools/developer-command-prompt-for-vs) in the project directory run the command:
`cl.exe /EHsc /std:c++17 MakerBotChallenge.cpp`

This should generate MakerBotChallenge.exe in the same directory which can be run from the command line.

## Execution

The program will ask the user for information about the models and displays the resulting model placements on screen.
It will also create an `output.svg` file which can be opened (i.e. in chrome) to visualize the results.

Notes on specific input requests:
*  build plate dimensions - Just the build plate size. Also used as the output.svg size. Recommended: "250 250" if you want the svg file to be reasonably large.
*  model generation mode - Use "manual" if you want to individually specify model dimensions for placement, "random" for random generation, and "seeded" for random generation with a specific seed.
*  maximum proportion of used space - This is only used for random model generation. It represents the target proportion of used space to unused space on the build plate.
Essentially a larger value here will make the models larger. Values close to "1" increase the chance of the algorithms failing to place the models in bounds. Recommended: ".7" will usually work.

For the most part I don't check for valid input because that wasn't the focus of the challenge. So don't stress it, it *will* break.

## Algorithms

At runtime, the program executes all the following algorithms. It only outputs the most optimal solution though.

### 1 - shelf algorithm

The first algorithm I wrote up involved a shelfing system. The plate is broken into 4 quadrants, each one acts like a bookcase.
Attempt to place each model on a shelf as close to the middle as possible.
Every time you add a model on a shelf, a new shelf is created above it for smaller models.

This is the most efficient algorithm by far, but can only occassionally generate more optimal results than the others (i.e 4 square models).

![shelf example](shelf_example.png)

### 2 - collision algorithm

When provided with more than 80 models, this algorithm is skipped to save runtime.

The slow but more optimal algorithm I wrote. It's very simple, place a model in the middle, and slide it until it finds a non-colliding spot.
Since you can slide along either axis, each step generates 2 more checks... which generate 2 more each and so on.
The legal placement closest to the center wins, and then you move on to place the next model.

This algorithm usually tightly packs, but it takes a lot longer when run on lots of small models.
By virtue of this assignment, the normal use case is only a handful of models, so runtime overhead isn't a big concern.

![collision example](collision_example.png)

### 2.5 - misaligned collision algorithm

When provided with more than 50 models, this algorithm is skipped to save runtime.

Identical to the above, except the first placed item is offset, so the corner is in the center. This up-front ineffeciency can save space later.
Primarily I put this variant in because for the case of 3 models neither of the other algorithms would ever get an optimal result.
This one can.

![misaligned example](misaligned_example.png)

### etc...

Each algorithm is ended by centering the final result. The centering algorithm draws the smallest circle around the models, and centering that circle.
The centering is much faster than placing, so it's not a big runtime concern.
The smallest circle is found using code I wrote implementing [Welzl's algorithm](https://en.wikipedia.org/wiki/Smallest-circle_problem#Welzl's_algorithm).
